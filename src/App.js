import React from 'react';
import './App.css';
import ProductContainer from './containers/productContainer';
import CartContainer from './containers/CartContainer';

function App() {
    return ( 
      <div className = "container" >
        <ProductContainer />

        <CartContainer />
      </div>
    );
}

export default App;