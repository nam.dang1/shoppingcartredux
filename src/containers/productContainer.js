import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as action from './../actions/index';
import Products from '../components/Products';
import ProductItem from './../components/ProductItem';

class ProductContainer extends Component {
    render() {
        let { products } = this.props;
        return ( 
            <Products>
                {this.showProducts(products)}
            </Products>
        );
    }
    showProducts(products){
        let result = null;
        let {addToCart} = this.props;
        result = products.map((product, index)=>{
            return  <ProductItem key = {index} product = {product} addToCart ={addToCart} />
        });
        return result;
    }
}


const stateToProps = (state) => {
    
    return {
        products: state.products
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addToCart: (product) => {
            dispatch(action.addToCart(product));
        }
    }
}
export default connect(stateToProps, mapDispatchToProps)(ProductContainer);