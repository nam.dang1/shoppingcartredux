import * as types from './../constants/ActionTypes'
let productsList = [{
        id: 1,
        name: 'Iphone',
        price: 100000,
        quantity: 20
    },
    {
        id: 2,
        name: 'Samsum J7Pro',
        price: 20000,
        quantity: 20
    },
    {
        id: 3,
        name: 'Iphone 7',
        price: 150000,
        quantity: 20
    }
]
let data = JSON.parse(localStorage.getItem('Products'));
let initialState = data ? data : productsList;
localStorage.setItem('Products', JSON.stringify(initialState));

export default function products(state = initialState, action) {

    switch (action.type) {
        case types.ADD_TO_CART:
            const productId = state.findIndex(products => products.id === action.product.id);

            if (state[productId].quantity > 0) {
                state[productId].quantity -= 1;
                localStorage.setItem('Products', JSON.stringify(state));
            }


            return [...state];

        default:
            return state;
    }
}