import * as types from './../constants/ActionTypes';

let data = JSON.parse(localStorage.getItem('cart'));
const initialState = data ? data : [];
export default function Cart(state = initialState, action) {
    switch (action.type) {
        case types.ADD_TO_CART:
            const result = state.findIndex(products => products.id === action.product.id);
            if (result === -1) {
                const newProductCart = {
                    id: action.product.id,
                    name: action.product.name,
                    price: action.product.price,
                    quantity: 1
                }
                state.push(newProductCart);
            } else {
                state[result].quantity += 1;
            }
            localStorage.setItem('cart', JSON.stringify(state));


            return [...state];
        default:
            return state;
    }
}