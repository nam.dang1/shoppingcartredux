import { combineReducers } from 'redux';
import products from './products';
import Cart from './Cart';
const rootReducer = combineReducers({
    products,
    Cart

});
export default rootReducer;