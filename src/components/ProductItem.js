import React, { Component } from 'react';
class ProductItem extends Component {
    render() {
        let {product} = this.props;
       
        return (
            <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3 productItem">
                <div className="thumbnail">
                    <img src="images/1.jpeg" alt="" />
                    <div className="caption">
                        <h3>{product.name}</h3>
                        <p> price: {product.price} </p>
                        <span>Quantity:{product.quantity }</span>
                        <p>
                            <button type="button" className="btn btn-primary" onClick={() =>  this.props.addToCart(product)}>Add to cart</button>
                           
                        </p>
                    </div>
                </div>
            </div>
        )
    }

    
}

export default  ProductItem;



