import React, { Component } from 'react';
class Carts extends Component {
    render() {
        return (
            <div className="cart">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Phone Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.children}
                    </tbody>
                </table>
        </div>

        );
    }
   
}

export default Carts;
