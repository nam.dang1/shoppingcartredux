import React, { Component } from 'react';
class Products extends Component {
    render() {
        return (
            <div className="products">
                <div className="row">
                    {this.props.children}
                </div>
            </div>
        );
    }
}
export default  Products;
