import * as types from './../constants/ActionTypes';
export const listAll = () => {
    return {
        type: types.LIST_ALL
    }
}
export const addToCart = (product) => {
    return {
        type: types.ADD_TO_CART,
        product

    }
}